/* 
 * File:   main.c
 * Author: Julie
 *
 * Created on 28 de julio de 2021, 11:12 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "fuses.h"

#define _XTAL_FREQ 4000000 // fr 4MHz

void main()
{
    TRISBbits.TRISB0 = 0;   // RB0 as Output PIN
    PORTBbits.RB0 = 0;
    
    while(1)
    {
        PORTBbits.RB0 = 1;  // LED On
        __delay_ms(1000);   // 1 Second Delay
        PORTBbits.RB0 = 0;  // LED Off
        __delay_ms(1000);
    }
}

