/*
 * File:   main.c
 * Author: Julie
 *
 * Created on 22 de septiembre de 2020, 09:42 PM
 */

#include <xc.h>
#include "fuses.h"

#define _XTAL_FREQ 20000000 // fr 20MHz

void main()
{
    TRISBbits.TRISB0 = 0;   // RB0 as Output PIN
    PORTBbits.RB0 = 0;
    
    while(1)
    {
        PORTBbits.RB0 = 1;  // LED ON
        __delay_ms(1000);   // 1 Second Delay
        PORTBbits.RB0 = 0;  // LED OFF
        __delay_ms(1000);   // 1 Second Delay
    }
}
